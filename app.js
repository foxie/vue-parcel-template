import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

/*
 * Makes it easier to call axios
 * See here https://github.com/imcvampire/vue-axios/issues/18
 */

import App from "/src/App.vue";

Vue.use(VueAxios, axios);

new Vue({
    el: "#app",
    render: h => h(App)
});
