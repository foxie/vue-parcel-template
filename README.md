# vue-parcel-template

A very basic template to get you started with your vue development with parcel.

## Getting started

Clone this repo.

Run
`yarn`

After yarn is all finnished with getting dependencies
`yarn dev`

This will open dev hotreload server on losalhost:1234 (if not taken).

If you want to build static site
`yarn build`


